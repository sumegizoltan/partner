﻿CREATE TABLE [dbo].[Table] (
    [Id]       INT           IDENTITY (1, 1) NOT NULL,
    [name]     NVARCHAR (50) NULL,
    [email]    NVARCHAR (50) NULL,
    [phone]    NVARCHAR (50) NULL,
    [modified] DATETIME      DEFAULT (getdate()) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

