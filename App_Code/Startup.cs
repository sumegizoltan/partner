﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(partner.Startup))]
namespace partner
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
