﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class _Default : Page
{
    SqlConnection conn = new SqlConnection(@"Data Source=(LocalDb)\v11.0;Initial Catalog=Partner;AttachDbFilename=|DataDirectory|\Partner.mdf;Integrated Security=SSPI");

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        SqlCommand cmd = null;

        conn.Open();
        cmd = conn.CreateCommand();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "INSERT INTO [Table] (name,email,phone) VALUES (@name, @email, @phone)";
        cmd.Parameters.AddWithValue("name", txtName.Text);
        cmd.Parameters.AddWithValue("email", txtEmail.Text);
        cmd.Parameters.AddWithValue("phone", txtPhone.Text);
        cmd.ExecuteNonQuery();
        conn.Close();
        Response.Redirect("Default.aspx");
    }
}