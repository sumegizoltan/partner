﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="About.aspx.cs" Inherits="About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <h3>Partnertörzs alkalmazás</h3>
    <p>Szerző</p>
    <address>
        Sümegi Zoltán<br />
        6528 Bátmonostor, Petőfi S. u. 37.<br />
        <abbr title="Phone">Tel.:</abbr>
        06-70-325-0745
    </address>

    <address>
        <strong>Email:</strong>   <a href="mailto:sumegizoltan73@gmail.com">sumegizoltan73@gmail.com</a>
    </address>
</asp:Content>
