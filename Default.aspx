﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Partnerek</h1>
    </div>

    <div class="row">
        <div class="col-md-8">
            <h2>Partneradatok</h2>
            <p>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                    SelectCommand="SELECT [Id], [name], [email], [phone], [modified] FROM [Table]"
                    UpdateCommand="UPDATE [Table] SET [name]=@name, [email]=@email, [phone]=@phone, [modified]=getdate() WHERE [Id]=@Id"
                    DeleteCommand="DELETE FROM [Table] WHERE [Id]=@Id"
                    DataSourceMode="DataReader" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>">
                </asp:SqlDataSource>

                <asp:GridView ID="grdData" runat="server"
                    DataSourceID="SqlDataSource1" 
                    DataKeyNames="Id" 
                    AutoGenerateColumns="False" 
                    AutoGenerateDeleteButton="True" 
                    AutoGenerateEditButton="True">
                    <Columns>
                        <asp:BoundField ReadOnly="True" HeaderText="ID" InsertVisible="False" DataField="Id" SortExpression="Id">
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Név" DataField="name" SortExpression="name"></asp:BoundField>
                        <asp:BoundField HeaderText="Email" DataField="email" SortExpression="email"></asp:BoundField>
                        <asp:BoundField HeaderText="Telefon" DataField="phone" SortExpression="phone"></asp:BoundField>
                        <asp:BoundField HeaderText="Módosítás dátuma" DataField="modified" SortExpression="modified" DataFormatString="{0:yyyy.MM.dd H:mm:ss}"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <h2>Új partner</h2>
            <div>
                <div class="row">
                    <div class="col-md-4">
                        Név
                    </div>
                    <div class="col-md-4">
                        Email
                    </div>
                    <div class="col-md-4">
                        Telefon
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
                    </div>
                </div>
                <asp:Button ID="Button1" CssClass="btn btn-default" runat="server" Text="Hozzáad &raquo;" OnClick="Button1_Click" />
            </div>
        </div>
    </div>
</asp:Content>
